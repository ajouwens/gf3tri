GF3Tri - Analog watch 
=====================

Description
-----------
Analog watch face with 3 'arms'.
--------------
Please, leave your comments, bug reports and ideas on this forum:
https://forums.garmin.com/showthread.php?265723-Watchfaces-GF3-
--------------
Version 1.0:
- First version.
--------------
