using Toybox.Application as App;

class GF3TriWatch extends App.AppBase {
    function onStart() {}

    function onStop() {}

    function getInitialView() {
        return [new GF3Tri()];
    }
}
