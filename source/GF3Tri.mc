using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;

class GF3Tri extends Ui.WatchFace {
    var pi9 = Math.PI / 9.0;
    var pi30 = Math.PI / 30.0;
    var cx = 109; // Just fenix3
    var cy = 109;
    var is24Hour = false;
    var circleCenter = [ [0, -159], [159, 0], [0, 159], [-159, 0] ];
    var hourHand = [ [-10, 0], [-10,-30], [0, -36], [10, -30], [10,0] ];

    function initialize() {
        is24Hour   = Sys.getDeviceSettings().is24Hour;
    }

    function onLayout(dc) {
        onUpdate(dc);
    }

    function onShow() {}

    function onHide() {}

    function onExitSleep() {}

    function onEnterSleep() {}

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_LT_GRAY, Gfx.COLOR_LT_GRAY);
        dc.clear();
        var battPerc = (Sys.getSystemStats().battery).toNumber();
        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var sec = Sys.getClockTime().sec;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_LONG);
        // Rotate the minute "hand"
        sec = sec * pi30;
        rotate(dc, sec, circleCenter);
        // Draw the left side
        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_DK_GRAY, Gfx.COLOR_DK_GRAY);
        dc.fillRectangle(0, 0, 109, 218);
        // Draw the hour hand
        drawHourHand(dc, sec, hourHand);
    }

    function rotate(dc, angle, coords) {
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + cx;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + cy;
            result[i] = [x, y];
        }
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT, Gfx.COLOR_WHITE);
        dc.fillCircle(result[0][0], result[0][1], 102);
        dc.fillCircle(result[1][0], result[1][1], 102);
        dc.fillCircle(result[2][0], result[2][1], 102);
        dc.fillCircle(result[3][0], result[3][1], 102);
    }

    function drawHourHand(dc, angle, coords) {
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + cx;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + cy;
            result[i] = [x, y];
        }
        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT, Gfx.COLOR_RED);
        dc.fillPolygon(result);
    }

    function drawSprocket(dc, dia, height) {
    }

}
